import { Component } from '@angular/core';
import { DataService } from '../services/data.service';

declare interface TableData {
    headerRow: string[];
    dataRows: string[][];
}

@Component({
    selector: 'icons-cmp',
    moduleId: module.id,
    templateUrl: 'icons.component.html'
})

export class IconsComponent{
    public tableData2: TableData;
    public linksQuebrados = [];
    public isLoading: boolean = true;

    constructor(public dataService: DataService)
    {
        this.dataService.linksQuebrados().then(data => {
            this.isLoading = false;
            for(let i=0; i<data['length']; i++){
                this.linksQuebrados.push([ data[i]['anime'], data[i]['episodio'] ])
                
            }
        }, error => {
            console.log(error);
        })
    }
    ngOnInit(){
        this.tableData2 = {
            headerRow: ['Nome', 'Episódio' ],
            dataRows: this.linksQuebrados
        };
    }

    apagarLink(link)
    {
        console.log(link);
        //this.dataService.logEvento(new Date().toLocaleString(), localStorage.getItem('currentUser'), 'Corrigiu o link quebrado: ');
    }
}
