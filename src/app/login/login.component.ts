import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('sucessoSwal') private sucessoSwal: SwalComponent;
  @ViewChild('erroSwal') private erroSwal: SwalComponent;
  public formLogin: FormGroup;
  public email;
  public senha;
  public swalError;

  constructor(public fb: FormBuilder, public afAuth: AngularFireAuth, public router: Router, public userService: UserService) 
  {
    this.formLogin = this.fb.group({
      email: [null, Validators.required],
      senha: [null, Validators.required]
    });
  }

  login(){
    this.userService.login(this.email, this.senha)
      .then(data => {
        console.log(data);
      }, error => {
        console.log(error);
      });
  }

  ngOnInit() {
  }

}
