import { Component, OnInit, ViewChild } from '@angular/core';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../services/data.service';

declare var $: any;

@Component({
    selector: 'notifications-cmp',
    moduleId: module.id,
    templateUrl: 'notifications.component.html'
})

export class NotificationsComponent implements OnInit {
    @ViewChild('sucessoSwal') private sucessoSwal: SwalComponent;
    @ViewChild('erroSwal') private erroSwal: SwalComponent;
    public result = [];
    public formEpisodio: FormGroup;
    public animeSelecionado;
    public numeroEpisodio;
    public urlEpisodio;
    public error;
    public enviarPush: boolean = false;
    constructor(public dataService: DataService, public fb: FormBuilder) {
        this.formEpisodio = this.fb.group({
            animeSelecionado: [null, Validators.required],
            numeroEpisodio: [null, Validators.required],
            urlEpisodio: [null, Validators.required],
            enviarPush: [false]
        });

        this.dataService.animes().then(letras => {
            for (let i = 0; i < letras['length']; i++) {
                let letra = Object.keys(letras[i]);
                for (let anime of letra) {
                    this.result.push({
                        nome: anime
                    });
                }
            }
        });
    }

    criar() {
        this.dataService.addEpisodio(this.animeSelecionado, this.numeroEpisodio, this.urlEpisodio)
            .then(() => {
                this.sucessoSwal.show();
                if (this.enviarPush) {
                    this.dataService.enviaPush(this.animeSelecionado, this.numeroEpisodio);
                }
                //this.dataService.showNotification('top', 'center', 'Episódio adicionado com sucesso, obrigado por sua colaboração', 'ti-check');
                this.dataService.logEvento(new Date().toLocaleString(), localStorage.getItem('currentUser'), 'Incluiu o ova: ' + this.numeroEpisodio + ' no anime: ' + this.animeSelecionado);
                this.numeroEpisodio = '';
                this.urlEpisodio = '';
                // se this.enviarPush === true, mandar push pela api oneSignal
            })
            .catch(error => {
                this.error = error;
                this.erroSwal.show();
            });
    }

    ngOnInit() { }
}
