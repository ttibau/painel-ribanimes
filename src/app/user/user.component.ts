import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../services/data.service';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';


@Component({
    selector: 'user-cmp',
    moduleId: module.id,
    templateUrl: 'user.component.html'
})

export class UserComponent implements OnInit {
    @ViewChild('sucessoSwal') private sucessoSwal: SwalComponent;
    @ViewChild('erroSwal') private erroSwal: SwalComponent;
    public result = [];
    public formEpisodio: FormGroup;
    public animeSelecionado;
    public numeroEpisodio;
    public urlEpisodio;
    public error; 
    public enviarPush: boolean = false;
    constructor(public dataService: DataService, public fb: FormBuilder)
    {
        this.formEpisodio = this.fb.group({
            animeSelecionado: [null, Validators.required],
            numeroEpisodio: [null, Validators.required],
            urlEpisodio: [null, Validators.required],
            enviarPush: [false]
        });

        this.dataService.animes().then(letras => {
            for(let i=0; i < letras['length']; i++){
                let letra = Object.keys(letras[i]);
                for(let anime of letra){
                    this.result.push({
                        nome: anime
                    });
                }
            }  
        });
    }
    
    ngOnInit() {

    }
    criar(){
        this.dataService.addEpisodio(this.animeSelecionado, this.numeroEpisodio, this.urlEpisodio)
        .then(() => {
            this.sucessoSwal.show();
            if(this.enviarPush){
                this.dataService.enviaPush(this.animeSelecionado, this.numeroEpisodio);
            }
            //this.dataService.showNotification('top', 'center', 'Episódio adicionado com sucesso, obrigado por sua colaboração', 'ti-check');
            this.dataService.logEvento(new Date().toLocaleString(), localStorage.getItem('currentUser'), 'Incluiu o episódio: ' + this.numeroEpisodio + ' no anime: ' + this.animeSelecionado);
            this.numeroEpisodio = '';
            this.urlEpisodio = '';
            // se this.enviarPush === true, mandar push pela api oneSignal
        })
        .catch(error => {
            this.error = error;
            this.erroSwal.show();
        });
    }

}
