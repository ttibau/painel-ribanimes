import { Component, ViewChild } from '@angular/core';
import { DataService } from '../services/data.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { SwalComponent } from '@toverux/ngx-sweetalert2';

@Component({
    selector: 'typography-cmp',
    moduleId: module.id,
    templateUrl: 'typography.component.html'
})

export class TypographyComponent{
    @ViewChild('sucessoSwal') private sucessoSwal: SwalComponent;
    @ViewChild('erroSwal') private erroSwal: SwalComponent;
    public letra;
    public nome;
    public genero;
    public icone; 
    public sinopse;
    public formAnime : FormGroup;
    public error;

    constructor(public dataService: DataService, public fb: FormBuilder)
    {
       this.formAnime = this.fb.group({
           nome: [null, Validators.required],
           genero: [null, Validators.required],
           icone: [null, Validators.required],
           sinopse: [null, Validators.required]
       })
    }

    addAnime() : void {
        this.letra = this.nome.charAt(0);
        this.dataService.addAnime(this.letra, this.nome, this.genero, this.icone, this.sinopse)
            .then(() => {
                this.sucessoSwal.show();
                this.dataService.logEvento(new Date().toLocaleString(), localStorage.getItem('currentUser'), 'Criou o anime: ' + this.nome);
                this.nome = '';
                this.genero = '';
                this.icone  = '';
                this.sinopse = '';
            })
            .catch((error) => {
                this.error = error;
                this.erroSwal.show();
            });
        
    }
}
