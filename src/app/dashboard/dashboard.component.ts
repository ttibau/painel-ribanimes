import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../services/data.service';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

declare var $:any;
declare interface TableData {
  headerRow: string[];
  dataRows: string[][];
}

@Component({
    selector: 'dashboard-cmp',
    moduleId: module.id,
    templateUrl: 'dashboard.component.html'
})

export class DashboardComponent implements OnInit{
  @ViewChild('confirmSwal') private sucessoSwal: SwalComponent;
  public tableData1: TableData;
  public tamanhoLinksQuebrados;
  public tamanhoUsuarios; 
  public countAnimes = 0;
  public ultimosEpisodios = [];
  public ultimosEpisodiosReverse = [];
  public episodioVideoUrl;
  public isLoading: boolean = true;
    constructor(public dataService: DataService, private modalService: NgbModal){
      this.isLoading = true;
      this.dataService.ultimosEpisodios()
      .then(data => {
        this.isLoading = false;
        for(let i=0; i < data['length']; i++){
          this.ultimosEpisodios.push([data[i]['nome'], data[i]['episodio']]);
        }
      })
      .catch(error => {
        console.log(error);
      })

      this.dataService.animes().then(letras => {
        for(let i=0; i < letras['length']; i++){
            let count = Object.keys(letras[i]).length;
            this.countAnimes += count;
        } 
      });
      
      this.dataService.linksQuebrados().then(data => {
        this.tamanhoLinksQuebrados = data['length'];
      });

      this.dataService.usuarios().then(data => {
        this.tamanhoUsuarios = data['length'];
      });

    
    } 

    abrirModal(content, row)
    {
      this.dataService.getEpisode(row).then(data => {
        this.episodioVideoUrl = data['url'];
        this.isLoading = false;
      }, error => {
        console.log(error);
      });
      this.modalService.open(content, { size: 'sm' });
    }

    ngOnInit(){
        
        this.tableData1 = {
          headerRow: [ 'Nome', 'Episódio', 'Opções'],
          dataRows: this.ultimosEpisodios
        };
    }

    apagar(episodio) 
    {
      this.dataService.apagarEpisodio(episodio).then(() => {
        this.dataService.logEvento(new Date().toLocaleString(), localStorage.getItem('currentUser'), 'Apagou o episódio: ' + episodio[1] + 'no anime: ' + episodio[0])
        window.location.reload();
      });
    }
}
