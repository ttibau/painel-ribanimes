import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { DataService } from '../services/data.service';
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SwalComponent } from '@toverux/ngx-sweetalert2';

declare interface TableData {
    headerRow: string[];
    dataRows: string[][];
}

@Component({
    selector: 'table-cmp',
    moduleId: module.id,
    templateUrl: 'table.component.html',
    encapsulation: ViewEncapsulation.None,
    styles: [
        `
        body.modal-open div.modal-backdrop { 
            z-index: 0 !important;
            -webkit-transform: translateZ(0) 
        }
        `
    ]
})

export class TableComponent implements OnInit{
    @ViewChild('confirmSwal') private sucessoSwal: SwalComponent;
    public tableData2: TableData;
    public listaAnimes = []; 
    public isLoading = true;
    closeResult: string;
    modalResult = {
        episodios: '',
        informacoes: ''
    };
    public isModalLoading = false;

    constructor(public dataService: DataService, private modalService: NgbModal) 
    {
        this.dataService.animes().then(letras => {
            this.isLoading = false;
            for(let i=0; i < letras['length']; i++){
                let letra = Object.keys(letras[i]);
                for(let anime of letra){
                   this.listaAnimes.push([anime]); 
                }
            }
        })
    }

    excluirAnime(anime)
    {
        this.dataService.excluirAnime(anime)
        .then(() => {
            this.dataService.logEvento(new Date().toLocaleString(), localStorage.getItem('currentUser'), 'Excluiu o anime: ' + anime);
        })
    }

    openLg(content, anime) {        
        let letra = anime.charAt(0);
        this.dataService.getAnimeFromKey(letra, anime).then(data => { 
            this.isModalLoading = true;
            this.modalResult = {
                episodios: data[0],
                informacoes: data[1]
            }
            this.modalService.open(content, { size: 'lg' });
        })
      }

    ngOnInit(){
        this.tableData2 = {
            headerRow: [ 'Nome' ],
            dataRows: this.listaAnimes
        };
    }
}
