import { Component } from '@angular/core';
import { UserService } from './services/user.service';

declare var $:any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent{
  isLogged;
  constructor(public userService: UserService){
    this.isLogged = localStorage.getItem('isLogged');
  }
}
