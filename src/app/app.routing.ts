import { Routes } from '@angular/router';

import { DashboardComponent }   from './dashboard/dashboard.component';
import { UserComponent }   from './user/user.component';
import { TableComponent }   from './table/table.component';
import { TypographyComponent }   from './typography/typography.component';
import { IconsComponent }   from './icons/icons.component';
import { MapsComponent }   from './maps/maps.component';
import { NotificationsComponent }   from './notifications/notifications.component';
import { UpgradeComponent }   from './upgrade/upgrade.component';
import { DevLogComponent } from './dev-log/dev-log.component';

export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    {
        path: 'dashboard',
        component: DashboardComponent
    },
    {
        path: 'novo-episodio',
        component: UserComponent
    },
    {
        path: 'animes',
        component: TableComponent
    },
    {
        path: 'novo-anime',
        component: TypographyComponent
    },
    {
        path: 'links-quebrados',
        component: IconsComponent
    },
    {
        path: 'logs',
        component: MapsComponent
    },
    {
        path: 'novo-ova',
        component: NotificationsComponent
    },
    {
        path: 'calendario',
        component: UpgradeComponent
    },
    {
        path: 'dev-log',
        component: DevLogComponent
    }
]
