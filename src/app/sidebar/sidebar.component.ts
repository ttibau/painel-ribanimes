import { Component, OnInit } from '@angular/core';

declare var $:any;

export interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}

export const ROUTES: RouteInfo[] = [
    { path: 'dashboard', title: 'Dashboard',  icon: 'ti-panel', class: '' },
    { path: 'novo-episodio', title: 'Novo Episódio',  icon:'ti-plus', class: '' },
    { path: 'novo-ova', title: 'Novo OVA',  icon:'ti-medall-alt', class: '' },
    { path: 'animes', title: 'Animes',  icon:'ti-view-list-alt', class: '' },
    { path: 'novo-anime', title: 'Novo Anime',  icon:'ti-plus', class: '' },
    { path: 'links-quebrados', title: 'L. Quebrados',  icon:'ti-pulse', class: '' },
    { path: 'logs', title: 'Logs',  icon:'ti-layout-list-thumb-alt', class: '' },
    { path: 'calendario', title: 'Calendário',  icon:'ti-calendar', class: 'active-pro' },
    { path: 'dev-log', title: 'Dev Log', icon: 'ti-shortcode', class: '' }
];

@Component({
    moduleId: module.id,
    selector: 'sidebar-cmp',
    templateUrl: 'sidebar.component.html',
    styles: [
        `
        .item-menu {
            margin: 20px;
        }
        `
    ]
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];
    public currentUser;
    ngOnInit() {
        this.currentUser = localStorage.getItem('currentUser');
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }
    isNotMobileMenu(){
        if($(window).width() > 991){
            return false;
        }
        return true;
    }

}
