import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Http, Headers, RequestOptions} from '@angular/http';
import * as firebase from 'firebase';
declare var $:any;

@Injectable({
  providedIn: 'root'
})
export class DataService {
  public result = 0;
  public animesArray: Array<any> = [];
  constructor(public db: AngularFireDatabase, public http: Http)
  { 
    
  }

  //Vai pegar o episódio
  getEpisode(episodio)
  {
    let letra = episodio[0].charAt(0);
    let anime = episodio[0];
    let epNumero = episodio[1];
    let promise = new Promise((resolve, reject) => {
      this.db.object('animes/' + letra + '/' + anime + '/episodios/' + epNumero).valueChanges()
        .subscribe(data => {
          resolve(data);
        }, error => {
          reject(error);
        })
    });
    return promise;
  }

  apagarEpisodio(episodio)
  {
    let letra = episodio[0].charAt(0);
    let anime = episodio[0];
    let epNumero = episodio[1];
    let promise = new Promise((resolve, reject) => {
      this.db.object('animes/' + letra + '/' + anime + '/episodios/' + epNumero).remove()
      .then(() => {
        // após apagar dos episódios, apagar também dos últimos episódios
        this.db.object('ultimosEpisodios/' + anime + ' - ' + epNumero).remove()
        .then(() => {
          this.showNotification('top', 'center', 'Episódio excluido com sucesso', 'ti-trash');
          resolve('sucesso');
        }, error => {
          console.log(error);
        })
      }, error => {
        reject(error);
      })
    })
    return promise;
  }

  excluirLink(link) 
  {
    
  }

  excluirAnime(anime)
  {
    let promise = new Promise((resolve, reject) => {
      let letra = anime.charAt(0);
      this.db.object('animes/' + letra + '/' + anime).remove()
      .then(() => {
        this.showNotification('top', 'center', 'Anime excluido com sucesso', 'ti-trash');
        resolve('sucesso');
      }, error => {
        reject(error);
      })
    });
    return promise;
  }

  getLogs() {
    let promise = new Promise((resolve, reject) => {
      this.db.list('logs', ref => ref.orderByChild('dateReverse').limitToFirst(25)).valueChanges().subscribe(data => {
        resolve(data.reverse());
      }, error => {
        reject(error);
      })
    });
    return promise;
  }

  logEvento(data, user, event) {
    const items = this.db.list('logs');
    const date = Date.now();
    items.push({
      user: user,
      data: data, 
      timestamp: date,
      dateReverse: 0 - date,
      event: event
    }).then(() => {
      this.showNotification('top', 'center', 'Evento salvo no log: ' +  event, 'ti-check')
    })  
  }

  enviaPush(anime, episodioNumero){
    let myHeaders = new Headers;
    myHeaders.append('Authorization', 'Basic MTQyNTk0YzEtYTZhNi00MjhiLThiN2MtOTFiZjExOGIyM2U2');
    myHeaders.append('Content-Type', 'application/json');
    let body = {
      "app_id": "2ac55b01-f584-40e0-abca-8628a78ecb70",
      "included_segments": ["All"],
      "data": {"foo": "bar"},
      "contents": {"en": anime + " - " + episodioNumero}
    }
    let options = new RequestOptions({ headers: myHeaders })
    this.http.post('https://onesignal.com/api/v1/notifications', body, options).subscribe(data => {
      console.log(data);
    }, error => {
      console.log(error);
    })
  }

  ultimosEpisodios()
  {
    let promise = new Promise ((resolve, reject) => {
      this.db.list('ultimosEpisodios', ref => ref.orderByChild('dateReverse').limitToFirst(15)).valueChanges().subscribe(data => {
         resolve(data);
      }, error => {
        reject(error);
      });
    });
    return promise;
  }

  addOva(anime, numero, url){
    let promise = new Promise ((resolve, reject) => {
      let letra = anime.charAt(0);
      let prev = '' // Prev é maior do que 0? Então inclui o prev, senao (se prev for 0), inclui nada
      let numeroNext = numero + 1;
      let numeroPrev = numero - 1;
      if(numero > 0){
        prev = 'animes/' + letra + '/' + anime + '/ovas/' + numeroPrev 
      } else {
        prev = '';
      }
      this.db.object('animes/' + letra + '/' + anime + '/ovas/' + numero).update({
        next: 'animes/' + letra + '/' + anime + '/ovas/' + numeroNext,
        prev: prev,
        titulo: numero,
        url: url
      }).then(sucesso => {
        resolve('sucesso');
      }).catch(error => {
        reject(error);
      })
    });
    return promise;
  }

  addEpisodio(anime, numero, url){
    let promise = new Promise ((resolve, reject) => {
      let letra = anime.charAt(0);
      let prev = '' // Prev é maior do que 0? Então inclui o prev, senao (se prev for 0), inclui nada
      let numeroNext = numero + 1;
      let numeroPrev = numero - 1;
      if(numero > 0){
        prev = 'animes/' + letra + '/' + anime + '/episodios/' + numeroPrev 
      } else {
        prev = '';
      }
      this.db.object('animes/' + letra + '/' + anime + '/episodios/' + numero).update({
        next: 'animes/' + letra + '/' + anime + '/episodios/' + numeroNext,
        prev: prev,
        titulo: numero,
        url: url
      }).then(sucesso => {
        // no sucesso da inserção do episódio, vou adicionar no nó 'novosEpisodios' o anime e o número do episódio
        const date = Date.now();
        this.db.object('ultimosEpisodios/' + anime + ' - ' + numero).update({
          nome: anime,
          episodio: numero,
          timestamp: date,
          dateReverse: 0 - date
        })
        resolve('sucesso');
      }).catch(error => {
        reject(error);
      })
    });
    return promise;
  }

  // Adiciona um novo anime
  addAnime(letra, nome, genero, icone, sinopse)
  {
    let promise = new Promise ((resolve, reject)=> {
      this.db.object('animes/' + letra + '/' + nome).update({
        informacoes: {
          nome: nome,
          genero: genero,
          icone: icone,
          sinopse: sinopse
        }
      }).then(sucesso => {
        resolve('sucesso');
      }).catch(error => {
        reject(error);
      });
    });
    return promise;
  }

  // Me retorna a quantidade de episódios
  countEpisodios()
  {
    this.animes().then(animes => {
      console.log(this.objectLength(animes))
    });
  }

  objectLength(obj) {
    var result = 0;
    for(var prop in obj) {
      if (obj.hasOwnProperty(prop)) {
      // or Object.prototype.hasOwnProperty.call(obj, prop)
        result++;
      }
    }
    return result;
  }

  getAnimeFromKey(letra, key){
    let promise = new Promise((resolve, reject) => {
      this.db.list('animes/' + letra + '/' + key).valueChanges().subscribe(data => {
        resolve(data);
      }, error => {
        reject(error);
      });
    });
    return promise;
  }

  // Me retorna os animes
  animes()
  {
    let promise = new Promise((resolve, reject) => {
      this.db.list('animes').valueChanges().subscribe(data => {
        resolve(data);
      }, error => {
        reject(error);
      });
    });
    return promise;
  }

  // Retorna os links quebrados
  linksQuebrados()
  {
    let promise = new Promise((resolve, reject) => {
      this.db.list('link-quebrado', ref=> ref.orderByChild('dateReverse')).valueChanges().subscribe(data => {
        resolve(data);
      }, error => {
        reject(error);
      })
    });
    return promise;
  }

  // retorna os usuários
  usuarios()
  {
    let promise = new Promise((resolve, reject) => {
      this.db.list('users').valueChanges().subscribe(data => {
        resolve(data);
      }, error => {
        reject(error);
      })
    });
    return promise;
  }


  showNotification(from, align, mensagem, icone){
    var type = ['','info','success','warning','danger'];

    //var color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: icone,
        message: mensagem
      },{
          type: 'success',
          timer: 4000,
          placement: {
              from: from,
              align: align
          }
      });
}
}
