import { Injectable, HostBinding } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  public user: Observable<firebase.User>;
  public isLogged;
  constructor(private afAuth: AngularFireAuth, private router: Router) {
    this.user = this.afAuth.authState;
   }


  login(email, senha)
  {
    let promise = new Promise((resolve, reject) => {
      this.afAuth.auth.signInWithEmailAndPassword(email, senha)
      .then(auth => {
        if(auth){
          resolve(auth);
          this.isLogged = true;
          this.router.navigateByUrl('/dashboard');
          localStorage.setItem('isLogged', 'true');
          localStorage.setItem('currentUser', auth.user.email);
        }
      }, error => {
        reject(error);
      })
    });
    return promise;
  }

  logout() 
  {
    this.afAuth.auth.signOut();
    localStorage.setItem('isLogged', 'false');
    this.router.navigateByUrl('/');
  }
  
}
