import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
declare interface TableData {
    headerRow: string[];
    dataRows: string[][];
}

@Component({
    moduleId: module.id,
    selector: 'maps-cmp',
    templateUrl: 'maps.component.html'
})

export class MapsComponent implements OnInit{
    public isLoading = true;
    public tableData2: TableData;
    public logs = []; 
    public logsReverse = [];
    constructor(public dataService: DataService)
    {
        this.dataService.getLogs().then(data => {
            this.isLoading = false;
            for(let i=0; i<data['length']; i++){
                this.logs.push([ data[i]['user'], data[i]['event'], data[i]['data'] ])
            }
            console.log(this.logs);
        }, error => {
            console.log(error);
        })
    }
    ngOnInit(){
       
        this.tableData2 = {
            headerRow: [ 'User', 'Descricao',  'Time' ],
            dataRows: this.logs
        };
    }
}

